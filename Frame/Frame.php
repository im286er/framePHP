<?php 
 Class Frame{
 	
 	
 	public static function original(){
 		/*1.定义常量*/
 		self::_Declaring();
 		//创建文件
 		self::_create_dir();
 		//加载必须文件
 		self::_load_file();
 		//执行应用函数
 		Adhibition::apply();
 	}

 	private static function _Declaring(){
 		//G:/wamp/www/framePHP/Frame/Frame.php
 		$path = str_replace('\\','/',__FILE__);
 		
 		//Frame 框架的根路径
 		//G:/wamp/www/framePHP/Frame/
 		define('FRAME_PATH',dirname($path));
 		//定义框架配置文件路径
 		
 		define('CONFIG_PATH',FRAME_PATH.'/Conf');
 		//定义数据存放文件夹
 		define('DATA_PATH', FRAME_PATH . '/Data');
 		//定义字体文件存放文件夹
		define('FONT_PATH', DATA_PATH . '/Font');
 		//定义框架模版文件路径
 		define('LIB_PATH', FRAME_PATH.'/Lib');
 		//定义扩展文件夹路径
 		define('EXTENDS_PATH', FRAME_PATH.'/Extends');
 		//定义工具文件路径9
 		define('TOOL_PATH', EXTENDS_PATH . '/Tool');
 		//定义框架的核心文件路径
		define('CORE_PATH', LIB_PATH . '/Core');
		//定义框架的Function 文件路径；
		define('FUNCTION_PATH', LIB_PATH . '/Function');

		//smarty 模版引擎的文件路径
		define('SMARTY_PATH', EXTENDS_PATH.'/Smarty');
 		

 		//定义项目的根路径
 		////G:/wamp/www/framePHP/
 		define('ROOT_PATH',dirname(FRAME_PATH));
 		//用户项目文件夹
 		define('APP_PATH',ROOT_PATH.'/'.APP_NAME);
 		//用户临时目录
 		//
 		define('APP_TEMP_PATH',APP_PATH.'/Temp');
 		// 用户应用编译文件
 		define('APP_COMPILE_PATH',APP_TEMP_PATH.'/compile');
 		//用户应用缓存文件
 		//
 		define('APP_CACHE_PATH',APP_TEMP_PATH.'/cache');	
 		//用户配置文件路径
 		define('APP_CONFIG_PATH',APP_PATH.'/Conf');


 		//用户模版路径
 		define('APP_TPL_PATH',APP_PATH.'/Tpl');
 		//定义PUBLIC 文件路径
 		define('APP_PUBLIC_PATH',APP_TPL_PATH.'/Public');

 		//用户控制文件路径
 		define('APP_CONTROL_PATH',APP_PATH.'/Control');

 		define('IS_POST', $_SERVER['REQUEST_METHOD'] == 'POST' ? true : false);

 		define('IS_AJAX',isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ? true : false);

}
/**
 * [_create_dir 创建文件夹]
 * @return [type] [description]
 */
		private static function _create_dir(){
			$create = array(
			//用户配置文件路径
			APP_CONFIG_PATH,
			//用户模版文件路径
			APP_TPL_PATH,
			//控制器文件
			APP_CONTROL_PATH,
			//存放应用样式文件，图片文件，
			APP_PUBLIC_PATH,
			//用户应用编译文件
			APP_COMPILE_PATH,
			//用户应用缓存文件
			APP_CACHE_PATH
			);
			//遍历建立
			foreach ($create as $v) {
				//如果不是文件路径，怎创建
				is_dir($v)||mkdir($v,0777,true);
			}
	}
		//加载必须文件
		private static function _load_file(){
			$load = array(
			//在框架的文件都必须提前建立好
			//function
			FUNCTION_PATH.'/function.php',
			//smarty 核心文件
			SMARTY_PATH.'/Smarty.class.php',
			//公用的应用类文件
			//
			//Smartyview 类文件
			//
			CORE_PATH.'/Smartyview.class.php',
			CORE_PATH.'/Action.class.php',
			//应用类文件（子级）
			CORE_PATH.'/Adhibition.class.php'


				);
			//存为一个数组，让他们遍历分别引入
			foreach ($load as $value) {
				require $value;
			}
		}
}
//总开关
Frame::original();
 ?>